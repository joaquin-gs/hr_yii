<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\employee */
/* @var $form ActiveForm */
?>
<div class="employee-employee">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'firstNameEn') ?>
        <?= $form->field($model, 'lastNameEn') ?>
        <?= $form->field($model, 'dob') ?>
        <?= $form->field($model, 'country') ?>
        <?= $form->field($model, 'hireDate') ?>
        <?= $form->field($model, 'phone') ?>
        <?= $form->field($model, 'positionId') ?>
        <?= $form->field($model, 'departmentId') ?>
        <?= $form->field($model, 'managerId') ?>
        <?= $form->field($model, 'numberHolidays') ?>
        <?= $form->field($model, 'firstNameKh') ?>
        <?= $form->field($model, 'lastNameKh') ?>
        <?= $form->field($model, 'address') ?>
        <?= $form->field($model, 'nationalIdNumber') ?>
        <?= $form->field($model, 'passportNumber') ?>
        <?= $form->field($model, 'gender') ?>
        <?= $form->field($model, 'active') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'note') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- employee-employee -->
