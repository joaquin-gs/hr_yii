<?php

namespace app\controllers;

use Yii;

class employeeController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionEmployee()
    {
        $model = new \app\models\employee();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('employee', ['model' => $model]);
    }
}
