<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $userId
 * @property int|null $employeeId
 * @property string $userName
 * @property string $userPassword
 * @property string|null $authKey
 * @property string|null $accessToken
 * @property string $created_at
 * @property string $deleted
 * @property string|null $deleted_by
 * 
 */
class Users extends ActiveRecord implements IdentityInterface
{

   /**
    * {@inheritdoc}
    */
   public static function tableName()
   {
      return 'users';
   }


   /**
    * {@inheritdoc}
    */
   public function rules()
   {
      return [
         [['employeeId'], 'integer'],
         [['userName', 'userPassword', 'created_at'], 'required'],
         [['userName'], 'string', 'max' => 40],
         [['userPassword', 'deleted_by'], 'string', 'max' => 60],
         [['authKey'], 'string', 'max' => 80],
         [['accessToken'], 'string', 'max' => 100],
         [['created_at'], 'safe'],
         [['deleted'], 'string', 'max' => 1],
         [['employeeId'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employeeId' => 'employeeId']],
      ];
   }


   /**
    * {@inheritdoc}
    */
   public function attributeLabels()
   {
      return [
         'userId' => 'User ID',
         'employeeId' => 'Employee ID',
         'userName' => 'User Name',
         'userPassword' => 'User Password',
         'authKey' => 'Auth Key',
         'accessToken' => 'Access Token',
         'created_at' => 'Created At',
         'deleted' => 'Deleted',
         'deleted_by' => 'Deleted By',
      ];
   }


   /**
    * {@inheritdoc}
    */
   public static function findIdentity($id)
   {
      return static::findOne($id);
   }


   /**
    * {@inheritdoc}
    */
   public static function findIdentityByAccessToken($token, $type = null)
   {
      return static::findOne(['access_token' => $token]);
   }


   /**
    * Finds user by username
    *
    * @param string $username
    * @return static|null
    */
   public static function findByUsername($username)
   {
      return Users::findOne(['userName' => $username]);
   }


   /**
    * {@inheritdoc}
    */
   public function getId()
   {
      return $this->userId;
   }


   /**
    * {@inheritdoc}
    */
   public function getAuthKey()
   {
      return $this->authKey;
   }


   /**
    * {@inheritdoc}
    */
   public function validateAuthKey($authKey)
   {
      return $this->authKey === $authKey;
   }


   /**
    * Validates password
    *
    * @param string $password password to validate
    * @return bool if password provided is valid for current user
    */
   public function validatePassword($password)
   {
      return $this->userPassword === $password;
   }


   public function beforeSave($insert)
   {
      if (parent::beforeSave($insert)) {
         if ($this->isNewRecord) {
            $this->authKey = \Yii::$app->security->generateRandomString();
         }
         return true;
      }
      return false;
   }
}
