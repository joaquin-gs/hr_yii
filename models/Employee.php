<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $employeeId
 * @property string $firstNameEn
 * @property string $lastNameEn
 * @property string|null $firstNameKh
 * @property string|null $lastNameKh
 * @property string $dob
 * @property string $country
 * @property string|null $address
 * @property string|null $nationalIdNumber
 * @property string|null $passportNumber
 * @property string $gender
 * @property string $hireDate
 * @property string|null $email
 * @property string $phone
 * @property int|null $departmentId
 * @property int|null $managerId
 * @property int $positionId
 * @property int $numberHolidays
 * @property string|null $note
 * @property string $active
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstNameEn', 'lastNameEn', 'dob', 'country', 'hireDate', 'phone', 'positionId'], 'required'],
            [['dob', 'hireDate'], 'safe'],
            [['departmentId', 'managerId', 'positionId', 'numberHolidays'], 'integer'],
            [['firstNameEn', 'lastNameEn', 'firstNameKh', 'lastNameKh'], 'string', 'max' => 30],
            [['country'], 'string', 'max' => 2],
            [['address'], 'string', 'max' => 200],
            [['nationalIdNumber', 'passportNumber'], 'string', 'max' => 20],
            [['gender', 'active'], 'string', 'max' => 1],
            [['email'], 'string', 'max' => 90],
            [['phone'], 'string', 'max' => 15],
            [['note'], 'string', 'max' => 600],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employeeId' => 'Employee ID',
            'firstNameEn' => 'First Name En',
            'lastNameEn' => 'Last Name En',
            'firstNameKh' => 'First Name Kh',
            'lastNameKh' => 'Last Name Kh',
            'dob' => 'Dob',
            'country' => 'Country',
            'address' => 'Address',
            'nationalIdNumber' => 'National Id Number',
            'passportNumber' => 'Passport Number',
            'gender' => 'Gender',
            'hireDate' => 'Hire Date',
            'email' => 'Email',
            'phone' => 'Phone',
            'departmentId' => 'Department ID',
            'managerId' => 'Manager ID',
            'positionId' => 'Position ID',
            'numberHolidays' => 'Number Holidays',
            'note' => 'Note',
            'active' => 'Active',
        ];
    }
}
