
-- --------------------------------------------------------

--
-- Table structure for table department
--

DROP TABLE IF EXISTS department;
CREATE TABLE IF NOT EXISTS department (
  departmentId tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  departmentName varchar(60) NOT NULL,
  deleted char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (departmentId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
