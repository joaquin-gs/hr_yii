--
-- Table structure for table employee
--

DROP TABLE IF EXISTS employee;
CREATE TABLE IF NOT EXISTS employee (
  employeeId smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  firstNameEn varchar(30) NOT NULL,
  lastNameEn varchar(30) NOT NULL,
  firstNameKh varchar(30) DEFAULT NULL,
  lastNameKh varchar(30) DEFAULT NULL,
  dob date NOT NULL,
  country char(2) NOT NULL,
  address varchar(200) DEFAULT NULL,
  nationalIdNumber varchar(20) DEFAULT NULL,
  passportNumber varchar(20) DEFAULT NULL,
  gender char(1) NOT NULL DEFAULT 'M',
  hireDate date NOT NULL,
  email varchar(90) DEFAULT NULL,
  phone varchar(15) NOT NULL,
  departmentId tinyint(3) UNSIGNED DEFAULT NULL,
  managerId smallint(5) UNSIGNED DEFAULT NULL,
  positionId tinyint(3) UNSIGNED NOT NULL,
  numberHolidays tinyint(4) UNSIGNED NOT NULL DEFAULT '12',
  note varchar(600) DEFAULT NULL,
  active char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (employeeId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
