
-- --------------------------------------------------------

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS position;
CREATE TABLE IF NOT EXISTS position (
  positionId tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  positionName varchar(90) NOT NULL,
  salary varchar(45) DEFAULT NULL,
  PRIMARY KEY (positionId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
