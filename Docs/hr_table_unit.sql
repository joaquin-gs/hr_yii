
-- --------------------------------------------------------

--
-- Table structure for table unit
--

DROP TABLE IF EXISTS unit;
CREATE TABLE IF NOT EXISTS unit (
  unitId tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  unitName varchar(80) NOT NULL,
  unitShortName varchar(20) DEFAULT NULL,
  managerId smallint(5) UNSIGNED NOT NULL,
  bedsQuantity tinyint(4) NOT NULL DEFAULT '0',
  deleted char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (unitId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
