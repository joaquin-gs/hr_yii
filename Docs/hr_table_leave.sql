
-- --------------------------------------------------------

--
-- Table structure for table `leave`
--

DROP TABLE IF EXISTS `leave`;
CREATE TABLE IF NOT EXISTS `leave` (
  employeeId smallint(6) NOT NULL,
  currentYear smallint(6) NOT NULL,
  gainAccrual smallint(6) DEFAULT NULL,
  fromPrevYear smallint(6) DEFAULT NULL,
  currentLeave smallint(6) DEFAULT NULL,
  publicHolidays smallint(6) DEFAULT NULL,
  sickLeave smallint(6) DEFAULT NULL,
  maternity smallint(6) DEFAULT NULL,
  PRIMARY KEY (employeeId,currentYear)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
