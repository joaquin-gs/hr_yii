
--
-- Constraints for dumped tables
--

--
-- Constraints for table commune
--
ALTER TABLE commune
  ADD CONSTRAINT FK_district_commune FOREIGN KEY (districtId) REFERENCES district (districtID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table district
--
ALTER TABLE district
  ADD CONSTRAINT FK_province_district FOREIGN KEY (provinceID) REFERENCES province (provinceId) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table village
--
ALTER TABLE village
  ADD CONSTRAINT FK_commune_village FOREIGN KEY (communeId) REFERENCES commune (communeId) ON DELETE NO ACTION ON UPDATE NO ACTION;
